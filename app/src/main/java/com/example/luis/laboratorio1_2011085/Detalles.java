package com.example.luis.laboratorio1_2011085;

import android.content.Intent;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class Detalles extends AppCompatActivity {
    private TextView txtTotal = null;
    private TextView txtPedido = null;
    private Button btnRegresar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles);
        txtPedido = (TextView) findViewById(R.id.txtPedido);
        txtTotal = (TextView) findViewById(R.id.txtTotal);
        Bundle bundle = getIntent().getExtras();
        txtPedido.setText(""+bundle.getString("pedido"));
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        Double total = Double.parseDouble(bundle.getString("pedido"))*5.50;
        txtTotal.setText(""+total);
        btnRegresar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startActivity(new Intent(Detalles.this,MainActivity.class));
            }

        });

    }
}
